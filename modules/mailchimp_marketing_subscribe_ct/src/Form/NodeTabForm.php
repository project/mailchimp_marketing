<?php

namespace Drupal\mailchimp_marketing_subscribe_ct\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\mailchimp_marketing\MailchimpInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Mailchimp campaign generation.
 */
class NodeTabForm extends FormBase {

  /**
   * The currently authenticated user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Mailchimp configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $configMailchimp;

  /**
   * Mailchimp configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $configMailchimpCt;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The mailchimp service.
   *
   * @var \Drupal\mailchimp_marketing\MailchimpInterface
   */
  protected MailchimpInterface $mailchimp;

  /**
   * Constructs a new NodeTabForm.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The currently authenticated user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   * @param \Drupal\mailchimp_marketing\MailchimpInterface $mailchimp
   *   The mailchimp service.
   */
  public function __construct(AccountInterface $current_user, ConfigFactoryInterface $config_factory, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger, EmailValidatorInterface $email_validator, MailchimpInterface $mailchimp) {
    $this->currentUser = $current_user;
    $this->configMailchimp = $config_factory->get('mailchimp_marketing.settings');
    $this->configMailchimpCt = $config_factory->get('mailchimp_marketing_subscribe_ct.settings');
    $this->messenger = $messenger;
    $this->logger = $logger->get('mailchimp_marketing_subscribe_ct');
    $this->emailValidator = $email_validator;
    $this->mailchimp = $mailchimp;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('logger.factory'),
      $container->get('email.validator'),
      $container->get('mailchimp_marketing.mailchimp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_marketing_subscribe_ct_node_tab';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    $mailchimpCampaign = $node->mailchimp_campaign->value;
    $mailchimpCampaignCreated = $node->mailchimp_campaign_created->value;
    $mailchimpCampaignLastSent = $node->mailchimp_campaign_last_sent->value;

    //$config = $this->config('simplenews.settings');
    $form['#title'] = $this->t('<em>Mailchimp campaign</em> @title', [
      '@title' => $node->getTitle(),
    ]);

    // We will need the node.
    $form_state->set('node', $node);

    $processed = [];
    $valuesReadOnlyToProcess = [
      'mailchimp_ct_campaign_template_from_name' => [
        'title' => 'Campaign from name',
        'type' => 'textfield',
        'description' => ''
      ],
      'mailchimp_ct_campaign_template_from_email' => [
        'title' => 'Campaign from email',
        'type' => 'textfield',
        'description' => 'Use verified domain. Domains such as @example.com will cause an exception.'
      ],
    ];
    $valuesToProcess = [
      'mailchimp_ct_campaign_template_subject' => ['title' => 'Campaign subject', 'type' => 'textfield', 'description' => ''],
      'mailchimp_ct_campaign_template_body' => ['title' => 'Campaign body', 'type' => 'textarea', 'description' => ''],
    ];

    $form['detailsro'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Details (read only)'),
      '#description' => $this->t('<p>Read only values.<br />Values can be overriden in the <a href=":settingsform">settings form</a></p>', [
        ':settingsform' => Url::fromRoute('mailchimp_marketing_subscribe_ct.admin')->toString(),
      ]),
    ];

    foreach ($valuesReadOnlyToProcess as $value => $details) {
      $processed[$value] = htmlspecialchars_decode(\Drupal::token()->replace($this->configMailchimpCt->get($value), [
        $node->getEntityType()->id() => $node,
      ], ['clear' => TRUE]), ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);

      $form['detailsro'][$value] = [
        '#type' => $details['type'],
        '#title' => $details['title'],
        '#description' => $details['description'],
        '#default_value' =>  $processed[$value],
        '#disabled' => FALSE,
      ];
    }

    $mapping = $this->configMailchimpCt->get('mailchimp_ct_mapping');
    $form['detailsro']['content_type_mapping'] = [
      '#type' => 'hidden',
      '#title' => $node->getType(),
      '#description' => 'Content type mapping to the mailchimp interest',
      '#default_value' =>  $mapping[$node->getType()],
    ];

    $form['details'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Details (update)'),
      '#description' => $this->t('<p>Only override details if required.<br />Templates can be overriden in the <a href=":settingsform">settings form</a></p>', [
        ':settingsform' => Url::fromRoute('mailchimp_marketing_subscribe_ct.admin')->toString(),
      ]),
    ];

    foreach ($valuesToProcess as $value => $details) {
      $processed[$value] = htmlspecialchars_decode(\Drupal::token()->replace($this->configMailchimpCt->get($value), [
        $node->getEntityType()->id() => $node,
      ], ['clear' => TRUE]), ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);

      $form['details'][$value] = [
        '#type' => $details['type'],
        '#title' => $details['title'],
        '#description' => $details['description'],
        '#default_value' =>  $processed[$value],
      ];
    }

    // Groups.
    $groupsSynced = $this->configMailchimpCt->get('mailchimp_ct_groups_sync');
    $groupsAllowed = $this->configMailchimpCt->get('mailchimp_ct_groups');
    $categories = Json::decode($groupsSynced);

    $form['details']['interests_head'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h3>Groups</h3><p>Content type group: <em>@group</em></p>', [
        '@group' => $this->configMailchimpCt->get('mailchimp_ct_group_id'),
      ]),
    ];

    if ($categories) {
      foreach ($categories as $categoryId => $category) {
        if (in_array($categoryId, array_keys($groupsAllowed))) {
          $form['details']['interests-' . $categoryId] = [
            '#title' => $category['title'],
            '#type' => 'checkboxes',
            '#options' => $category['item'],
          ];
        }
      }
    }

    // Show newsletter sending options if newsletter has not been sent yet.
    // If send a notification is shown.
    $form['test'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => $this->t('Test'),
    ];

    $form['test']['test_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test email addresses'),
      '#description' => $this->t('A comma-separated list of email addresses to be used as test addresses.'),
      '#default_value' => $this->currentUser->getEmail(),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['test']['details_test'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p><strong>Test campaign details:</strong>:<br />Test campaign ID: <em>@campaign</em><br />Test campaign created: <em>@created</em></p>', [
        '@campaign' => ($mailchimpCampaign) ?? 'NA',
        '@created' => ($mailchimpCampaignCreated) ? DrupalDateTime::createFromTimestamp($mailchimpCampaignCreated, DateTimeItemInterface::STORAGE_TIMEZONE)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT) : 'Never',
      ]),
    ];

    $form['test']['submit'] = [
      '#type' => 'submit',
      '#value' => $mailchimpCampaign ? $this->t('Update email body and send test email') : $this->t('Create campaign and send test email'),
      '#name' => 'send_test',
      '#submit' => ['::sendTestCampaign'],
      '#validate' => ['::validateTestAddress'],
    ];

    if ($mailchimpCampaign) {
      $form['test']['delete'] = [
        '#type' => 'submit',
        '#button_type' => 'danger',
        '#value' => $this->t('DELETE test campaign from mailchimp'),
        '#submit' => ['::deleteCampaign'],
      ];
    }

    $form['send'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Send'),
    ];

    $form['send']['details_sent'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Campaign last sent: <em>@sent</em></p>', [
        '@sent' => ($mailchimpCampaignLastSent) ? DrupalDateTime::createFromTimestamp($mailchimpCampaignLastSent, DateTimeItemInterface::STORAGE_TIMEZONE)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT) : 'Never',
      ]),
    ];

    $send_text = $this->t('Send campaign(s)');
    if ($mailchimpCampaignLastSent) {
      $send_text = $this->t('Resend campaign(s)');
    }

    $revisionId = $node->getRevisionId();
    if (isset($revisionId)) {
      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $storage->resetCache([$node->id()]);
      /** @var \Drupal\node\NodeInterface $original */
      $original = $storage->loadRevision($revisionId);

    }

    $form['send']['mailchimp_campaign_create_on_publish'] = [
      '#type' => 'checkbox',
      '#title' => t('Schedule campaign to everyone when published'),
      '#default_value' => $node->mailchimp_campaign_create_on_publish->value,
      '#description' => t('Mailchimp campaign will be sent when node is published next.<br /><strong>Note</strong> sending campaign when next published does not support topics.'),
    ];

    $form['send']['send'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $send_text,
    ];

    return $form;
  }

  /**
   * Validates the test address.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function validateTestAddress(array $form, FormStateInterface $form_state) {
    $test_address = trim($form_state->getValue('test_address'));
    if (!empty($test_address)) {
      $mails = explode(',', $test_address);

      foreach ($mails as $mail) {
        $mail = trim($mail);
        if (!$this->emailValidator->isValid($mail)) {
          $form_state->setErrorByName('test_address', $this->t('Invalid email address "%mail".', ['%mail' => $mail]));
        }
      }
      $form_state->set('test_addresses', $mails);
    }
    else {
      $form_state->setErrorByName('test_address', $this->t('Missing test email address.'));
    }
  }

  /**
   * Submit handler for sending test mails.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function sendTestCampaign(array &$form, FormStateInterface $form_state) {
    $mailchimp = $this->mailchimp->getConnection();
    $request = NULL;
    $response = NULL;
    $campaignId = NULL;
    $node = $form_state->get('node');
    $mailchimpCampaignId = $node->mailchimp_campaign->value;

    try {
      if ($mailchimpCampaignId) {
        $response = $mailchimp->campaigns->get($mailchimpCampaignId);
      }
    }
    catch (\Exception $error) {
      $campaignId = NULL;
        $node
        ->set('mailchimp_campaign', NULL)
        ->set('mailchimp_campaign_created', NULL)
        ->save();
    }

    try {
      // Create campaign.
      if (!$campaignId) {
        $rules = [
          'interests-' . $this->configMailchimpCt->get('mailchimp_ct_group_id') => [
            "op" => "interestcontains",
            "value" => [
              $form_state->getValue('content_type_mapping'),
            ],
          ],
        ];

        $audience_id = $this->configMailchimpCt->get('mailchimp_ct_audience_id');
        $subject = $form_state->getValue('mailchimp_ct_campaign_template_subject');
        $body = $form_state->getValue('mailchimp_ct_campaign_template_body');
        $from_name = $form_state->getValue('mailchimp_ct_campaign_template_from_name');
        $from_email = $form_state->getValue('mailchimp_ct_campaign_template_from_email');
        $campaignId = $this->createCampaign($audience_id, $from_name, $from_email, $subject, $body, $rules);
      }
      else {
        $mailchimp->campaigns->setContent($campaignId, [
          //'html' => str_replace('[CONTENT]', $campaign['html'], $campaignTemplate),
          'html' => $form_state->getValue('mailchimp_ct_campaign_template_body'),
        ]);
      }

      $response = $mailchimp->campaigns->sendTestEmail($campaignId, [
        "test_emails" => explode(',', $form_state->getValue('test_address')),
        "send_type" => "html",
      ]);

      $node
        ->set('mailchimp_campaign_created', \Drupal::time()->getRequestTime())
        ->set('mailchimp_campaign', $campaignId)
        ->save();

      $message = $this->t('Test campaign was sent: Campaign id: @id.', [
        '@id' => $campaignId,
      ]);

      $this->logger->notice($message);
      $this->messenger->addStatus($message);
    }
    catch (\Exception $error) {
      $message = $this->t('Error message: @message<br />Details: @details<br /> Campaign details: <br /><pre>@campaign</pre><br /> Request details: <br /><pre>@request</pre>', [
        '@message' => $this->t('Send test campaign: Failed.'),
        '@details' => $error->getMessage(),
        '@campaign' => print_r($campaignId, TRUE),
        '@request' => print_r($request, TRUE),
      ]);

      $this->logger->error($message);
      $this->messenger->addError($message);
    }
  }

  /**
   * Handler for creating mailchimp campaign.
   *
   * @param string $audience_id
   *   Mailchimp audience ID.
   * @param string $from_name
   *   From name for campaign.
   * @param string $from_email
   *   From email for campaign.
   * @param string $subject
   *   Subject for campaign.
   * @param string $body
   *   Body for campaign.
   * @param array $rules
   *   Array of segments.
   *
   * @return string|NULL
   *   Mailchimp request id or NULL of fail.
   */
  public function createCampaign($audience_id, $from_name, $from_email, $subject, $body, $rules) {
    // = $this->mailchimp->getConnection();
    $campaignId = NULL;

    if ($campaignId = $this->mailchimp->createCampaign($audience_id, $from_name, $from_email, $subject, $body, $rules)) {
      $this->messenger->addStatus('Campaign was successfully created.');
    }

    return $campaignId;
  }

  /**
   * Handler for deleting mailchimp campaign.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function deleteCampaign(array &$form, FormStateInterface $form_state) {
    $mailchimp = $this->mailchimp->getConnection();
    $node = $form_state->get('node');
    $campaignId = $node->mailchimp_campaign->value;
    $response = NULL;

    try {
      if ($campaignId) {
        $response = $mailchimp->campaigns->remove($campaignId);
      }

      $node
        ->set('mailchimp_campaign_created', NULL)
        ->set('mailchimp_campaign', NULL)
        ->save();

      $this->messenger->addStatus('Test campaign was successfully deleted.');

    }
    catch (\Exception $error) {
      $campaignError = TRUE;
      $message = $this->t('Error: @message<br />Details: @details<br /> Campaign details: <br /><pre>@campaign</pre><br /> Response: <br /><pre>@response</pre>', [
        '@message' => $this->t('Failed to delete campaign.'),
        '@details' => $error->getMessage(),
        '@campaign' => print_r($campaignId, TRUE),
        '@response' => print_r($response, TRUE),
      ]);

      $this->logger->error($message);
      $this->messenger->addError($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $node = $form_state->get('node');

    // When mailchimp_campaign_create_on_publish is checked.
    if ($form_state->getValue('mailchimp_campaign_create_on_publish')) {
      $node
        ->set('mailchimp_campaign_create_on_publish', TRUE)
        ->save();

      $message = $this->t('Campaign will be sent next when published.');
      $this->messenger->addStatus($message);

      return;
    }

    $interests = array_filter($form_state->getValues(), function ($item, $key) {
      return (strpos($key, 'interests-') === 0);
    }, ARRAY_FILTER_USE_BOTH);

    $flatInterests = array_merge(...array_values($interests));

    $selectedInterests = array_filter($flatInterests, function ($item) {
      return ($item !== 0);
    });

    $mailchimp = $this->mailchimp->getConnection();
    $request = NULL;
    $response = NULL;
    $campaignIds = [];

    $contentTypeRule = [
      'interests-' . $this->configMailchimpCt->get('mailchimp_ct_group_id') => [
        "op" => "interestcontains",
        "value" => [
          $form_state->getValue('content_type_mapping'),
        ],
      ],
    ];

    try {
      // Create main campaign.
      $rules = $contentTypeRule;
      foreach ($interests as $id => $values) {
        if (count(array_intersect($values, $selectedInterests))) {
          $rules[$id] = [
            "op" => "interestcontains",
            "value" => array_filter(array_values($values), function ($item) {
              return ($item !== 0);
            }),
          ];
        }
      }

      $audience_id = $this->configMailchimpCt->get('mailchimp_ct_audience_id');
      $from_name = $this->configMailchimpCt->get('mailchimp_ct_campaign_template_from_name');
      $from_email = $this->configMailchimpCt->get('mailchimp_ct_campaign_template_from_email');
      $subject = $form_state->getValue('mailchimp_ct_campaign_template_subject');
      $body = $form_state->getValue('mailchimp_ct_campaign_template_body');
      $campaignIds[] = $this->createCampaign($audience_id, $from_name, $from_email, $subject, $body, $rules);

      // Create secondary campaign.
      if (count($selectedInterests) &&
        ($this->configMailchimpCt->get('mailchimp_ct_target_no_categories'))
      ) {
        // Create main campaign.
        $rules = $contentTypeRule;
        foreach ($interests as $id => $values) {
          if (count(array_intersect($values, $selectedInterests))) {
            $rules[$id] = [
              "op" => "interestnotcontains",
              "value" => array_keys($values),
            ];
          }
        }

        $campaignIds[] = $this->createCampaign($audience_id, $from_name, $from_email, $subject, $body, $rules);
      }

      // Schedule or send campaign.
      $scheduled = FALSE;
      foreach ($campaignIds as $campaignId) {
        if ($this->configMailchimp->get('campaign_schedule')) {
          $scheduled = TRUE;
          $scheduleTime = $this->configMailchimp->get('campaign_schedule_time');
          $datetime = new \DateTime($scheduleTime);
          $datetime->setTimezone(new \DateTimeZone('UTC'));
          $hours = $datetime->format('H');
          $minutes = (int) $datetime->format('i');
          $minutes = $minutes - ($minutes % 15) + 15;
          $datetime->setTime($hours, $minutes, 0);
          $mailchimp->campaigns->schedule($campaignId, [
            'schedule_time' => $datetime->format('Y-m-d\TH:i:s\Z'),
          ]);
        }
        else {
          $mailchimp->campaigns->send($campaignId);
        }
      }

      $node
        ->set('mailchimp_campaign_last_sent', \Drupal::time()->getRequestTime())
        ->set('mailchimp_campaign_create_on_publish', FALSE)
        ->save();

      $message = $this->t('@campaign @action: @ids.', [
        '@campaign' => (count($campaignIds) == 1) ? $this->t('Campaign was') : $this->t('Campaigns were'),
        '@action' => ($scheduled) ? $this->t('scheduled') : $this->t('sent'),
        '@ids' => implode(', ', $campaignIds),
      ]);
      $this->logger->notice($message);
      $this->messenger->addStatus($message);
    }
    catch (\Exception $error) {
      $message = $this->t('Error message: @message<br />Details: @details<br /> Campaign details: <br /><pre>@campaign</pre><br /> Request details: <br /><pre>@request</pre>', [
        '@message' => $this->t('Send test campaign: Failed.'),
        '@details' => $error->getMessage(),
        '@campaign' => print_r($campaignId, TRUE),
        '@request' => print_r($request, TRUE),
      ]);

      $this->logger->error($message);
      $this->messenger->addError($message);
    }
  }

  /**
   * Checks access for the mailchimp node tab.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node where the tab should be added.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   An access result object.
   */
  public function checkAccess(NodeInterface $node) {
    $mapping = $this->configMailchimpCt->get('mailchimp_ct_mapping');

    return AccessResult::allowedIf(is_array($mapping) &&
      in_array($node->getType(), array_keys($mapping)) &&
      ($this->currentUser()->hasPermission('send mailchimp campaign')));
  }

}
