<?php

namespace Drupal\mailchimp_marketing_subscribe_ct\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\mailchimp_marketing\Form\ImmutableConfigFormBaseTrait;
use Drupal\mailchimp_marketing\MailchimpInterface;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Mailchimp marketing settings.
 */
class MailchimpSubscribeCtSettingsForm extends ConfigFormBase {
  use ImmutableConfigFormBaseTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The mailchimp service.
   *
   * @var \Drupal\mailchimp_marketing\MailchimpInterface
   */
  protected MailchimpInterface $mailchimp;

  /**
   * Constructs a new MailchimpSubscribeCtSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\mailchimp_marketing\MailchimpInterface $mailchimp
   *   The mailchimp service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, MessengerInterface $messenger, LoggerInterface $logger, MailchimpInterface $mailchimp) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->mailchimp = $mailchimp;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('logger.channel.mailchimp_marketing'),
      $container->get('mailchimp_marketing.mailchimp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_marketing_subscribe_ct_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mailchimp_marketing_subscribe_ct.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->getEditableConfig();

    $immutable_message = $this->t('<br /><strong>This field is READ ONLY.</strong>');

    if (!$this->mailchimp->pingSuccess()) {
      $config_url = new Url('mailchimp_marketing.admin');

      $this->messenger->addError(
        $this->t('Error when connecting to mailchimp. Please check the API key. %link.', [
          '%link' => Link::fromTextAndUrl($this->t('Configure mailchimp marketing settings'), $config_url)->toString(),
        ])
      );

      $form['actions']['submit']['#attributes']['disabled'] = 'disabled';
      return $form;
    }

    $ct_group_id = empty($form_state->getValue('mailchimp_ct_group_id')) ? $this->getConfigValue('mailchimp_ct_group_id') : $form_state->getValue('mailchimp_ct_group_id');
    $ct_audience_id = empty($form_state->getValue('mailchimp_ct_audience_id')) ? $this->getConfigValue('mailchimp_ct_audience_id') : $form_state->getValue('mailchimp_ct_audience_id');
    $ct_mapping = empty($form_state->getValue('mailchimp_ct_mapping')) ? $this->getConfigValue('mailchimp_ct_mapping') : $form_state->getValue('mailchimp_ct_mapping');

    $lists = [0 => 'Select audience'];
    $mailchimp = $this->mailchimp->getConnection();
    $response = $mailchimp->lists->getAllLists();
    foreach ($response->lists as $list) {
      $list_id = $list->id;
      $list_name = $list->name;
      $lists[$list_id] = $list_name;
    }

    $form['ct_audience_id'] = [
      '#required' => TRUE,
      '#type' => 'select',
      '#title' => $this->t('Audience'),
      '#description' => $this->t('Audience list for content type subscribers.'),
      '#options' => $lists,
      '#default_value' => $ct_audience_id,
      '#ajax' => [
        'callback' => [$this, 'loadCategoriesOptions'],
        'disable-refocus' => TRUE,
        'event' => 'change',
        'wrapper' => 'refresh-ct-group-id',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading data...'),
        ],
      ],
    ];

    if ($this->isConfigValueDifferent('newsletter_list_id')) {
      $form['newsletter_list_id']['#description'] .= $immutable_message;
    }

    // Container for categories dropdown for Ajax callback.
    $form['container_ct_group_id'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'refresh-ct-group-id'],
    ];

    // Checking if categories are from different account.
    $categories = [];
    try {
      if ($ct_audience_id) {
        $categories = $this->mailchimp->getCategories($ct_audience_id, []);
      }
    }
    catch (ClientException $e) {
      $error = $this->t('Error getting mailchimp categories: <br /><pre>@error</pre>', ['@error' => $e->getMessage()]);
      $this->logger->error($error);
      $this->messenger->addError($error);
    }

    // @todo Fix validation.
    $form['container_ct_group_id']['ct_group_id'] = [
      //'#required' => TRUE,
      '#type' => 'select',
      '#title' => $this->t('Group'),
      '#description' => $this->t('Group to match content types (in Mailchimp audience: Manage contacts -> Groups).'),
      '#options' => $categories,
      '#default_value' => $ct_group_id,
      /*'#ajax' => [
        'callback' => [$this, 'loadInterestsOptions'],
        'disable-refocus' => TRUE,
        'event' => 'change',
        'wrapper' => 'refresh-newsletter-interest-id',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading data...'),
        ],
      ],*/
    ];

    // Container for categories dropdown for Ajax callback.
    $form['container_ct_types'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#attributes' => ['id' => 'refresh-ct-mapping'],
      '#title' => $this->t('Content types mappings'),
    ];

    if ($this->isConfigValueDifferent('mailchimp_ct_group_id')) {
      $form['container_ct_group_id']['ct_group_id']['#description'] .= $immutable_message;
    }

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    // Checking if interests are from different account.
    $groupItems = [];
    try {
      $groupItems = $this->mailchimp->getGroupItems($ct_audience_id, $ct_group_id, [0 => 'Select matching group item']);

      // @todo: sort content type labels.
      foreach ($types as $key => $type) {
        $form['container_ct_types']['content_type_' . $key] = [
          '#type' => 'select',
          '#title' => $type->label(),
          '#options' => $groupItems,
          '#default_value' => $ct_mapping[$key] ?? 0,
        ];
      }
    }
    catch (ClientException $e) {
      $error = $this->t('Error getting mailchimp groups: <br /><pre>@response</pre>', ['@response' => $response]);
      $this->logger->error($error);
      $this->messenger->addError($error);
    }

    $form['mailchimp_ct_campaign_template_from_name'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Campaign from name'),
      '#description' => $this->t('Campaign from name.'),
      '#default_value' => $config->get('mailchimp_ct_campaign_template_from_name'),
    ];

    $form['mailchimp_ct_campaign_template_from_email'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Campaign from email'),
      '#description' => $this->t('Campaign from email'),
      '#default_value' => $config->get('mailchimp_ct_campaign_template_from_email'),
    ];

    $form['mailchimp_ct_campaign_template_subject'] = [
      '#required' => TRUE,
      '#type' => 'textfield',
      '#title' => $this->t('Campaign subject'),
      '#description' => $this->t('Campaign template subject. Available tokens: '),
      '#default_value' => $config->get('mailchimp_ct_campaign_template_subject'),
    ];

    $form['mailchimp_ct_campaign_template_body'] = [
      '#required' => TRUE,
      '#type' => 'textarea',
      '#title' => $this->t('Campaign Template Body'),
      '#description' => $this->t('Campaign Template Body. Available tokens: '),
      '#default_value' => $config->get('mailchimp_ct_campaign_template_body'),
    ];

    $syncCategories = $categories;
    try {
      foreach ($categories as $groupId => $category) {
        $syncCategories[$groupId] = [
          'title' => $category,
          'item' => $this->mailchimp->getGroupItems($ct_audience_id, $groupId),
        ];
      }

      $decodedCategories = Json::encode($syncCategories);

      // Container for categories dropdown for Ajax callback.
      $form['mailchimp_ct_groups_sync'] = [
        '#type' => 'details',
        '#open' => FALSE,
        '#title' => $this->t('Mailchimp groups'),
      ];

      $form['mailchimp_ct_groups_sync']['mailchimp_ct_groups_sync'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Groups (Read only)'),
        '#description' => $this->t('Syncronised mailchimp groups'),
        '#default_value' => $decodedCategories,
      ];

      $mailchimp_ct_groups_sync_period_options = [
        0 => 'Next cron run',
        1800 => '30 minutes',
        3600 => '1 hour',
        7200 => '2 hours',
        10800 => '3 hours',
        21600 => '6 hours',
        43200 => '12 hours',
        86400 => '1 day',
        604800 => '1 week',
      ];

      $form['mailchimp_ct_groups_sync']['mailchimp_ct_groups_sync_period'] = [
        '#type' => 'select',
        '#title' => $this->t('Mailchimp groups sync period'),
        '#description' => $this->t('Timeframe to sync groups (should be larger than cron run period).'),
        '#default_value' => $config->get('mailchimp_ct_groups_sync_period'),
        '#options' => $mailchimp_ct_groups_sync_period_options,
      ];

      unset($categories[$ct_group_id]);
      $form['mailchimp_ct_groups'] = [
        '#multiple' => TRUE,
        '#type' => 'select',
        '#title' => $this->t('Additional Groups'),
        '#description' => $this->t('Which groups to show and send to on send tab. make sure not to select major group from above.'),
        '#default_value' => $config->get('mailchimp_ct_groups'),
        '#options' => $categories,
      ];
    }
    catch (ClientException $e) {
      $error = $this->t('Error getting mailchimp groups: <br /><pre>@response</pre>', ['@response' => $response]);
      $this->logger->error($error);
      $this->messenger->addError($error);
    }

    $form['mailchimp_ct_target_no_categories'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Target no categories subscription'),
      '#description' => $this->t('Generate additional campaign for content type for subscribers with no categories.'),
      '#default_value' => $config->get('mailchimp_ct_target_no_categories'),
    ];

    $form['#validate'][] = [$this, 'validateForm'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = array_filter($form_state->getValues(), function ($value, $key) {
      return ($value != '0') && (strpos($key, 'content_type_') === 0);
    }, ARRAY_FILTER_USE_BOTH);

    // @todo Fix validation.
    if (empty($values)) {
      //$form_state->setErrorByName('ct_group_id', $this->t('Match at least one content type below.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $audienceId = $form_state->getValue('ct_audience_id') ?? '';
    $groupId = $form_state->getValue('ct_group_id') ?? '';

    $mappings = array_filter($form_state->getValues(), function ($value, $key) {
      return ($value != '0') && (strpos($key, 'content_type_') === 0);
    }, ARRAY_FILTER_USE_BOTH);
    $mappingValues = [];
    foreach ($mappings as $key => $mapping) {
      $mappingValues[str_replace('content_type_', '', $key)] = $mapping;
    }

    $config = $this->getEditableConfig();
    $config->set('mailchimp_ct_audience_id', $audienceId);
    $config->set('mailchimp_ct_group_id', $groupId);
    $config->set('mailchimp_ct_mapping', $mappingValues);
    $config->set('mailchimp_ct_campaign_template_from_name', $form_state->getValue('mailchimp_ct_campaign_template_from_name'));
    $config->set('mailchimp_ct_campaign_template_from_email', $form_state->getValue('mailchimp_ct_campaign_template_from_email'));
    $config->set('mailchimp_ct_campaign_template_subject', $form_state->getValue('mailchimp_ct_campaign_template_subject'));
    $config->set('mailchimp_ct_campaign_template_body', $form_state->getValue('mailchimp_ct_campaign_template_body'));
    $config->set('mailchimp_ct_groups_sync', $form_state->getValue('mailchimp_ct_groups_sync'));
    $config->set('mailchimp_ct_groups_sync_period', $form_state->getValue('mailchimp_ct_groups_sync_period'));
    $config->set('mailchimp_ct_groups_sync_timestamp', \Drupal::time()->getRequestTime());
    $config->set('mailchimp_ct_groups', $form_state->getValue('mailchimp_ct_groups'));
    $config->set('mailchimp_ct_target_no_categories', $form_state->getValue('mailchimp_ct_target_no_categories'));
    $config->save();

    $this->messenger()
      ->addStatus($this->t('The configuration options have been saved.'));
  }

  /**
   * Form submission handler for testing connection.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitFormTestConnection(array &$form, FormStateInterface $form_state) {
    try {
      $mailchimp = $this->mailchimp->getConnection();
      $response = print_r($mailchimp->ping->get(), TRUE);

      $this->messenger->addStatus($this->t('Mailchimp Response: <br /><pre>@response</pre>',
        ['@response' => $response])
      );
    }
    catch (ClientException $e) {
      $this->messenger->addError($this->t('Mailchimp exception: <br /><pre>@exception</pre>', [
        '@exception' => $e->getMessage(),
      ]));
    }
  }

}
