<?php

/**
 * @file
 * The Mailchimp Marketing content type subscription module.
 */

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Implements hook_help().
 */
function mailchimp_marketing_subscribe_ct_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.mailchimp_marketing_subscribe_ct':
      $output = '<p>' . t('Mailchimp marketing module with ability to subscribe to content type.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_entity_base_field_info().
 */
function mailchimp_marketing_subscribe_ct_entity_base_field_info(EntityTypeInterface $entity_type) {
  if ($entity_type->id() == 'node') {
    $fields = [];

    $fields['mailchimp_campaign'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Mailchimp campaign ID'))
      ->setDescription(t('Mailchimp test campaign ID.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 1,
      ]);

    $fields['mailchimp_campaign_last_sent'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Mailchimp campaign last sent'))
      ->setDescription(t('Mailchimp campaign last sent timestamp.'))
      ->setRevisionable(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['mailchimp_campaign_created'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Mailchimp test campaign created'))
      ->setDescription(t('Mailchimp test campaign created timestamp.'))
      ->setRevisionable(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['mailchimp_campaign_create_on_publish'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Send when node is published'))
      ->setDescription(t('Mailchimp campaign will be sent when node is published.'))
      ->setRevisionable(TRUE);

    return $fields;
  }
}

/**
 * Implements hook_cron().
 */
function mailchimp_marketing_subscribe_ct_cron() {
  // Sync groups if it is time.
  $mailchimp = \Drupal::service('mailchimp_marketing.mailchimp');
  $config = \Drupal::config('mailchimp_marketing_subscribe_ct.settings');
  $requestTime = \Drupal::time()->getRequestTime();
  $syncTimestamp = $config->get('mailchimp_ct_groups_sync_timestamp');
  $syncPeriod = $config->get('mailchimp_ct_groups_sync_period');

  if (($syncPeriod == 0) ||
    (($syncPeriod > 0) && ($syncTimestamp + $syncPeriod < $requestTime))
  ) {
    $syncCategories = [];
    try {
      $ctAudienceId = $config->get('mailchimp_ct_audience_id');

      if ($ctAudienceId) {
        $categories = $mailchimp->getCategories($ctAudienceId, []);

        foreach ($categories as $groupId => $category) {
          $syncCategories[$groupId] = [
            'title' => $category,
            'item' => $mailchimp->getGroupItems($ctAudienceId, $groupId),
          ];
        }

        $decodedCategories = Json::encode($syncCategories);
        if ($decodedCategories != $config->get('mailchimp_ct_groups_sync')) {
          $config->set('mailchimp_ct_groups_sync', $decodedCategories);
          $config->set('mailchimp_ct_groups_sync_timestamp', $requestTime);
          $config->save();
        }
      }
    }
    catch (ClientException $e) {
      $error = t('Error getting mailchimp categories: <br /><pre>@error</pre>', ['@error' => $e->getMessage()]);
      \Drupal::logger('mailchimp_marketing_subscribe_ct')->error($error);
    }
  }

}

/**
 * Implements hook_ENTITY_TYPE_presave() for node.
 */
function mailchimp_marketing_subscribe_ct_node_presave(EntityInterface $node) {
  $sendCampaign = FALSE;
  $mailchimp = \Drupal::service('mailchimp_marketing.mailchimp');
  $config = \Drupal::config('mailchimp_marketing_subscribe_ct.settings');
  $logger = \Drupal::logger('mailchimp_marketing_subscribe_ct');

  $storage = \Drupal::entityTypeManager()->getStorage('node');
  $revisions = $storage->revisionIds($node);
  if ($revisions && (count($revisions) > 1)) {
    // For nodes with workflow.
    $latestRevisionIds = array_slice($revisions, -2, 2, TRUE);
    $last = $storage->loadRevision(end($latestRevisionIds));
    $secondLast = $storage->loadRevision(reset($latestRevisionIds));
    $sendCampaign = ($last->isPublished() && !$secondLast->isPublished() &&
      $node->mailchimp_campaign_create_on_publish->value
    );
  }
  else {
    // For nodes without workflow.
    $sendCampaign = ($node->original && !$node->original->isPublished() &&
      $node->isPublished() && $node->mailchimp_campaign_create_on_publish &&
      $node->mailchimp_campaign_create_on_publish->value);
  }

  if ($sendCampaign) {
    // @todo: refactor duplicate code between this function and NodeTabForm.
    $audience_id = $config->configMailchimpCt->get('mailchimp_ct_audience_id');
    $from_name = $config->configMailchimpCt->get('mailchimp_ct_campaign_template_from_name');
    $from_email = $config->configMailchimpCt->get('mailchimp_ct_campaign_template_from_email');
    $subject = htmlspecialchars_decode(\Drupal::token()->replace($config->get('mailchimp_ct_campaign_template_subject'), [
      $node->getEntityType()->id() => $node,
    ], ['clear' => TRUE]), ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);
    $body = htmlspecialchars_decode(\Drupal::token()->replace($config->get('mailchimp_ct_campaign_template_body'), [
      $node->getEntityType()->id() => $node,
    ], ['clear' => TRUE]), ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);
    $mapping = $config->get('mailchimp_ct_mapping');

    // Generate campaigns.
    $contentTypeRule = [
      'interests-' . $config->get('mailchimp_ct_group_id') => [
        "op" => "interestcontains",
        "value" => [
          $mapping[$node->getType()],
        ],
      ],
    ];

    try {
      // Create main campaign.
      // @todo: support extra topics on later publishing.
      $rules = $contentTypeRule;
      $campaignIds[] = $mailchimp->createCampaign($audience_id, $from_name, $from_email, $subject, $body, $rules);

      // Send campaign (do not schedule).
      foreach ($campaignIds as $campaignId) {
        $mailchimp->campaigns->send($campaignId);
      }

    $node
      ->set('mailchimp_campaign_create_on_publish', FALSE)
      ->set('mailchimp_campaign_last_sent', \Drupal::time()->getRequestTime());

      $message = t('@campaign @action: @ids.', [
        '@campaign' => (count($campaignIds) == 1) ? t('Campaign was') : t('Campaigns were'),
        '@action' => t('sent'),
        '@ids' => implode(', ', $campaignIds),
      ]);
      $logger->notice($message);
    }
    catch (\Exception $error) {
      $message = t('Error message: @message<br />Details: @details<br /> Campaign details: <br /><pre>@campaign</pre>', [
        '@message' => t('Send test campaign: Failed.'),
        '@details' => $error->getMessage(),
        '@campaign' => print_r($campaignId, TRUE),
      ]);

      $logger->error($message);
    }
  }
}
