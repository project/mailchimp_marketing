<?php

namespace Drupal\mailchimp_marketing\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with a mailchimp subscription form block.
 *
 * @Block(
 *   id = "mailchimp_subscription_form_block",
 *   admin_label = @Translation("Mailchimp Subscription Form"),
 * )
 */
class MailchimpSubscriptionFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'form_mailchimp_html' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $value = $this->configuration['form_mailchimp_html'];
    return [
      '#markup' => Markup::create($value),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['form_mailchimp_html'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mailchimp form HTML'),
      '#description' => $this->t('Head to mailchimp: <code>Audience > Signup forms > Embedded forms</code> and copy HTML.'),
      '#default_value' => $config['form_mailchimp_html'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['form_mailchimp_html'] = $form_state->getValue('form_mailchimp_html');
  }
}
