<?php

namespace Drupal\mailchimp_marketing\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\mailchimp_marketing\MailchimpInterface;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Mailchimp marketing settings.
 */
class MailchimpMarketingSettingsForm extends ConfigFormBase {
  use ImmutableConfigFormBaseTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The mailchimp service.
   *
   * @var \Drupal\mailchimp_marketing\MailchimpInterface
   */
  protected $mailchimp;

  /**
   * Constructs a new NegotiationUrlForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\mailchimp_marketing\MailchimpInterface $mailchimp
   *   The mailchimp service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, MessengerInterface $messenger, LoggerInterface $logger, MailchimpInterface $mailchimp) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->mailchimp = $mailchimp;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('logger.channel.mailchimp_marketing'),
      $container->get('mailchimp_marketing.mailchimp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailchimp_marketing_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mailchimp_marketing.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $immutable_message = $this->t('<br /><strong>This value is READ ONLY.</strong>');

    $mc_api_url = Url::fromUri('http://admin.mailchimp.com/account/api', ['attributes' => ['target' => '_blank']]);
    $form['mailchimp_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('mailchimp_api_key'),
      '#description' => $this->t('The API key for your Mailchimp account. Get or generate a valid API key at your @apilink.', ['@apilink' => Link::fromTextAndUrl($this->t('Mailchimp API Dashboard'), $mc_api_url)
          ->toString()]) . ($this->isConfigValueDifferent('mailchimp_api_key') ? $immutable_message : ''),
    ];

    if ($this->mailchimp->pingSuccess()) {
      $form['mailchimp_server_prefix'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#type' => 'markup',
        '#markup' => $this->t('<strong>Server Prefix (based on API key)</strong>: @server_prefix', [
          '@server_prefix' => $this->getConfigValue('mailchimp_server_prefix'),
        ]),
      ];

      $lists = [];
      $mailchimp = $this->mailchimp->getConnection();
      $response = $mailchimp->lists->getAllLists();
      foreach ($response->lists as $list) {
        $list_id = $list->id;
        $list_name = $list->name;
        $lists[$list_id] = $list_name;
      }

      $form['mailchimp_default_list'] = [
        '#type' => 'select',
        '#title' => $this->t('Default list'),
        '#options' => $lists,
        '#default_value' => $this->getConfigValue('mailchimp_default_list'),
        '#description' => $this->isConfigValueDifferent('mailchimp_default_list') ? $immutable_message : '',
      ];

      $form['actions']['mailchimp_test'] = [
        '#weight' => 10,
        '#type' => 'submit',
        '#value' => 'Test Connection',
        '#submit' => ['::submitFormTestConnection'],
      ];

    }
    else {
      $this->messenger->addError($this->t('Error when connecting to mailchimp. Please check the API key.'));
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Generate prefix.
    $api_key = $form_state->getValue('mailchimp_api_key');
    $api_key_exploded = explode('-', $api_key);
    $server_prefix = end($api_key_exploded);
    $default_list = $form_state->getValue('mailchimp_default_list');

    $immutableConfig = [];
    $config = $this->getEditableConfig();

    if (!$this->isConfigValueDifferent('mailchimp_api_key')) {
      $config->set('mailchimp_api_key', $api_key);
      $config->set('mailchimp_server_prefix', $server_prefix);
    }
    else {
      $immutableConfig[] = "\$config['mailchimp_marketing.settings']['mailchimp_api_key'] = '" . $api_key . "';";
      $immutableConfig[] = "\$config['mailchimp_marketing.settings']['mailchimp_server_prefix'] = '" . $server_prefix . "';";
    }
    if (!$this->isConfigValueDifferent('mailchimp_default_list')) {
      $config->set('mailchimp_default_list', $default_list);
    }
    else {
      $immutableConfig[] = "\$config['mailchimp_marketing.settings']['mailchimp_default_list'] = '" . $default_list . "';";
    }
    $config->save();
    parent::submitForm($form, $form_state);

    $this->messenger()
      ->addStatus($this->t('The configuration options have been saved.'));
    if (count($immutableConfig)) {
      $this->messenger()
        ->addStatus($this->t('Some values might be stored in settings file and require overrides in <code>settings.php</code>: <br />@settings', [
          '@settings' => implode("\n", $immutableConfig),
        ]));
    }

  }

  /**
   * Form submission handler for testing connection.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitFormTestConnection(array &$form, FormStateInterface $form_state) {
    try {
      $mailchimp = $this->mailchimp->getConnection();
      $response = print_r($mailchimp->ping->get(), TRUE);

      $this->messenger->addStatus($this->t('Mailchimp Response: <br /><pre>@response</pre>',
        ['@response' => $response])
      );
    }
    catch (ClientException $e) {
      $this->messenger->addError($this->t('Mailchimp exception: <br /><pre>@exception</pre>', [
        '@exception' => $e->getMessage(),
      ]));
    }
  }

}
