<?php

namespace Drupal\mailchimp_marketing\Form;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides access to read only configuration for forms.
 *
 * This trait provides a configRead() method.
 */
trait ImmutableConfigFormBaseTrait {

  /**
   * Gets a read only configuration object.
   *
   * @param string $name
   *   The name of the configuration object to retrieve. The name corresponds to
   *   a configuration file. For @code \Drupal::config('book.admin') @endcode,
   *   the config object returned will contain the contents of book.admin
   *   configuration file.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   An editable configuration object if the given name is listed in the
   *   getEditableConfigNames() method or an immutable configuration object if
   *   not.
   */
  protected function configRead($name) {
    if (method_exists($this, 'configFactory')) {
      /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
      $config_factory = $this->configFactory();
    }
    elseif (property_exists($this, 'configFactory')) {
      $config_factory = $this->configFactory;
    }
    if (!isset($config_factory) || !($config_factory instanceof ConfigFactoryInterface)) {
      throw new \LogicException('No config factory available for ConfigFormBaseTrait');
    }
    $config = $config_factory->get($name);
    return $config;
  }

  /**
   * Retrieves a read only configuration object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   An immutable configuration object.
   */
  protected function getReadOnlyConfig() {
    if (method_exists($this, 'getEditableConfigNames')) {
      $config_names = $this->getEditableConfigNames();
      $config_name = reset($config_names);
      return $this->configRead($config_name);
    }
    else {
      return $this->configRead($this->name);
    }
  }

  /**
   * Retrieves a configuration object.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   An editable configuration object if the given name is listed in the
   *   getEditableConfigNames() method or an immutable configuration object if
   *   not.
   */
  protected function getEditableConfig() {
    $config_names = $this->getEditableConfigNames();
    $config_name = reset($config_names);
    return $this->config($config_name);
  }

  /**
   * Retrieves a read only configuration object.
   *
   * @param string $name
   *   The name of the configuration property to compare.
   *
   * @return bool
   *   TRUE if immutable config value is different to mutable. This usually
   *   indicates that values are overridden in settings.
   */
  protected function isConfigValueDifferent($name) {
    return $this->getEditableConfig()->get($name) != $this->getReadOnlyConfig()->get($name);
  }

  /**
   * Retrieves a read only configuration object.
   *
   * @param string $name
   *   The name of the configuration property to compare.
   *
   * @return mixed
   *   Configuration value that was requested.
   */
  protected function getConfigValue($name) {
    return ($this->isConfigValueDifferent($name)) ? $this->getReadOnlyConfig()->get($name) : $this->getEditableConfig()->get($name);
  }

}
