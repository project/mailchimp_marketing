<?php

namespace Drupal\mailchimp_marketing;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Mailchimp controller.
 */
interface MailchimpInterface {

  /**
   * Constructs a new MailChimpInterface object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The Logger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $validator
   *   The email validator.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $config_factory, EmailValidatorInterface $validator);

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container);

  /**
   * Mailchimp connection getter.
   *
   * @return \MailchimpMarketing\ApiClient
   *   Mailchimp connection object.
   */
  public function getConnection();

  /**
   * Test connection.
   *
   * @return bool
   *   TRUE if connection successful.
   */
  public function pingSuccess();

  /**
   * Test connection.
   *
   * @return bool
   *   TRUE if connection successful.
   */
  public function getDefaultList();

  /**
   * Checks if user is member of the list by email.
   *
   * @param string $email
   *   Value of email.
   * @param string $listId
   *   Value of Mailchimp list id.
   *
   * @return object|bool
   *   Member object or FALSE if not found.
   */
  public function getMember($email, $listId = NULL);

  /**
   * Checks if user is member of the list by unique_email_id.
   *
   * @param string $uniqueId
   *   Value of unique_email_id.
   * @param string $listId
   *   Value of Mailchimp list id.
   *
   * @return object|bool
   *   Member object or FALSE if not found.
   */
  public function getMemberByUniqueId(string $uniqueId, $listId = NULL);

  /**
   * Add email to mailchimp list.
   *
   * @param string $email
   *   Value of email.
   * @param string $listId
   *   Value of Mailchimp list id.
   * @param array $options
   *   Other options for request, example:
   *   [
   *     "email_type" => "html",
   *     "tags": [],
   *     "interests" => ["8833943587" => TRUE, "6e79a9a8c9" => FALSE],
   *   ].
   *    See
   *   https://mailchimp.com/developer/api/marketing/list-members/add-member-to-list/
   *   for full list of parameters.
   * @param string $memberStatus
   *   Status of member, 'subscribed' is default, but if double opt-in is
   *   required set it to 'pending'.
   *
   * @return object|bool
   *   Member object or FALSE if failed to add a new subscriber.
   */
  public function addMember(string $email, string $listId = NULL, array $options = [], string $memberStatus = 'subscribed');

  /**
   * Load categories options values.
   *
   * @param string $listId
   *   Mailchimp list id.
   * @param array $categories
   *   Default category array (optional).
   *
   * @return array
   *   List of categories.
   */
  public function getCategories(string $listId, array $categories = ['none' => 'Select item']): array;

  /**
   * Load group items.
   *
   * @param string $list_id
   *   Mailchimp list id.
   * @param string $group_id
   *   Mailchimp group id.
   * @param array $group_items
   *   Mailchimp group id.
   *
   * @return array
   *   List of group items.
   */
  public function getGroupItems(string $list_id, string $group_id, array $group_items = ['none' => 'Select item']): array;

  /**
   * Create mailchimp campaign.
   *
   * @param string $audience_id
   *   Mailchimp audience ID.
   * @param string $from_name
   *   From name for campaign.
   * @param string $from_email
   *   From email for campaign.
   * @param string $subject
   *   Subject for campaign.
   * @param string $body
   *   Body for campaign.
   * @param array $rules
   *   Array of segments.
   *
   * @return string|NULL
   *   Mailchimp request id or NULL of fail.
   */
  public function createCampaign($audience_id, $from_name, $from_email, $subject, $body, $rules);

}
