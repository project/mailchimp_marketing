<?php

namespace Drupal\mailchimp_marketing;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use MailchimpMarketing\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Mailchimp services.
 */
class Mailchimp implements MailchimpInterface {

  use StringTranslationTrait;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The Logger channel service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritDoc}
   */
  public function __construct(LoggerChannelFactoryInterface $logger, ConfigFactoryInterface $config_factory, EmailValidatorInterface $validator) {
    $this->logger = $logger->get('mailchimp_marketing');
    $this->config = $config_factory->get('mailchimp_marketing.settings');
    $this->emailValidator = $validator;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): Mailchimp {
    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger */
    $logger = $container->get('logger.factory');
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    /** @var \Drupal\Component\Utility\EmailValidatorInterface $validator */
    $validator = $container->get('email.validator');

    return new static(
      $logger,
      $config_factory,
      $validator
    );
  }

  /**
   * {@inheritDoc}
   */
  public function pingSuccess() {
    try {
      $mailchimp = $this->getConnection();
      $response = $mailchimp->ping->get();
      return ($response->health_status === "Everything's Chimpy!");
    }
    catch (ClientException $e) {
      return FALSE;
    }
    catch (ConnectException $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getConnection() {
    $api_key = $this->config->get('mailchimp_api_key');
    $server_prefix = $this->config->get('mailchimp_server_prefix');

    // Test the connection.
    $mailchimp = new ApiClient();
    $mailchimp->setConfig([
      'apiKey' => $api_key,
      'server' => $server_prefix,
    ]);

    return $mailchimp;
  }

  /**
   * {@inheritDoc}
   */
  public function getDefaultList() {
    return $this->config->get('mailchimp_default_list');
  }

  /**
   * {@inheritDoc}
   */
  public function getMemberByUniqueId(string $uniqueId, $listId = NULL) {
    $mailchimp = $this->getConnection();

    if (empty($uniqueId)) {
      $this->logger->notice("Invalid unique email id.");
      return FALSE;
    }

    if (!isset($listId)) {
      $listId = $this->config->get('mailchimp_default_list');
    }

    if (!isset($listId)) {
      $this->logger->warning("Mailchimp list is undefined.");
      return FALSE;
    }

    try {
      $response = $mailchimp->lists->getListMembersInfo($listId, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, $uniqueId);
      if ($response->total_items > 0) {
        $response = $response->members[0];
      }
      else {
        return FALSE;
      }
    }
    catch (Exception $exception) {
      $this->logger->error($exception->getMessage());
      return FALSE;
    }

    return $response;
  }

  /**
   * {@inheritDoc}
   *
   * Groups: $options['interests'] = ["8833943587" => TRUE, "6e79a9a8c9" =>
   * FALSE];
   */
  public function addMember(string $email, string $listId = NULL, array $options = [], string $memberStatus = 'subscribed') {
    if (!isset($listId)) {
      $listId = $this->config->get('mailchimp_default_list');

      if (empty($listId)) {
        $this->logger->warning("Mailchimp list is undefined.");
        return FALSE;
      }
    }

    $member = $this->getMember($email, $listId);
    if (!$member) {
      // Add new member.
      $request = [
        "email_address" => $email,
        "status" => $memberStatus,
      ];

      if (isset($options)) {
        $request = array_merge($request, $options);
      }

      try {
        $mailchimp = $this->getConnection();

        // To successfully add tags to new user, custom keys should be removed.
        if (isset($request['tags']) && is_array($request['tags'])) {
          $mailchimp_tags_no_ids = [];
          foreach ($request['tags'] as $mailchimp_tag) {
            $mailchimp_tags_no_ids[] = $mailchimp_tag;
          }
          $request['tags'] = $mailchimp_tags_no_ids;
        }

        $member = $mailchimp->lists->addListMember($listId, $request);
      }
      catch (Exception $exception) {
        $this->logger->error($exception->getMessage());
        return FALSE;
      }
    }
    else {
      // Check user's status.
      if ($member->status === 'unsubscribed') {
        $this->logger->error($this->t('User has unsubscribed from this mail list and cannot be re-subscribed.'));
        return FALSE;
      }

      if ($member->status === 'pending') {
        $this->logger->error($this->t('User has subscribed to this list, but has not confirmed their subscription.'));
        return FALSE;
      }

      // Update existing subscriber.
      try {
        $mailchimp = $this->getConnection();
        if (isset($options) &&
          (isset($options['tags']) && count($options['tags']))
        ) {

          // Update tags.
          $requestTags = [];
          $mailchimpTags = $mailchimp->lists->getListMemberTags($listId, $member->id, NULL, NULL, 200);
          $tags = $options['tags'];

          // Set members tags from mailchimp to inactive.
          // @todo remove tags if options is empty.
          foreach ($mailchimpTags->tags as $tag) {
            if (!in_array($tag->name, $tags)) {
              $requestTags[] = [
                "name" => $tag->name,
                "status" => 'inactive',
              ];
            }
          }

          // Set new tags to active.
          foreach ($tags as $tag) {
            $requestTags[] = [
              "name" => $tag,
              "status" => "active",
            ];
          }

          $request = [
            "tags" => $requestTags,
          ];

          $mailchimp->lists->updateListMemberTags(
            $listId,
            $member->id,
            $request
          );
          unset($options['tags']);
        }

        if (isset($options) &&
          (isset($options['interests']) && count($options['interests']))
        ) {
          // Update groups.
          $interests = [];
          foreach ($member->interests as $interestId => $interestFlag) {
            $interests[strval($interestId)] = FALSE;
          }

          $interests = $options['interests'] + $interests;
          $options['interests'] = $interests;
        }

        if (!empty($options)) {
          $mailchimp->lists->updateListMember(
            $listId,
            $member->id,
            $options
          );
        }
      }
      catch (Exception $exception) {
        $this->logger->error($exception->getMessage());
        return FALSE;
      }
    }

    return isset($member) ? $member : FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getMember($email, $listId = NULL) {
    $mailchimp = $this->getConnection();

    if (!$this->emailValidator->isValid($email)) {
      $this->logger->notice("Invalid email address.");
      return FALSE;
    }

    if (!isset($listId)) {
      $listId = $this->config->get('mailchimp_default_list');
    }

    if (!isset($listId)) {
      $this->logger->warning("Mailchimp list is undefined.");
      return FALSE;
    }

    try {
      $memberId = md5(strtolower($email));
      $response = $mailchimp->lists->getListMember($listId, $memberId);
    }
    catch (Exception $exception) {
      $this->logger->warning($exception->getMessage());
      return FALSE;
    }

    return $response;
  }

  /**
   * Load categories options values.
   *
   * @param string $listId
   *   Mailchimp list id.
   * @param array $categories
   *   Default category array.
   *
   * @return array
   *   List of categories.
   */
  public function getCategories(string $listId, array $categories = ['none' => 'Select item']): array {
    if (!empty($listId)) {
      $mailchimp = $this->getConnection();
      $response = $mailchimp->lists->getListInterestCategories($listId);
      foreach ($response->categories as $category) {
        $category_id = $category->id;
        $category_name = $category->title;
        $categories[$category_id] = $category_name;
      }
    }

    return $categories;
  }

  /**
   * Load group items.
   *
   * @param string $list_id
   *   Mailchimp list id.
   * @param string $group_id
   *   Mailchimp group id.
   * @param array $group_items
   *   Default group items e.g. ['none' => 'Select item'].
   *
   * @return array
   *   List of group items.
   */
  public function getGroupItems(string $list_id, string $group_id, array $group_items = []): array {
    if (!empty($list_id) && !empty($group_id)) {
      $mailchimp = $this->getConnection();
      $response = $mailchimp->lists->listInterestCategoryInterests($list_id, $group_id);
      foreach ($response->interests as $interest) {
        $id = $interest->id;
        $title = $interest->name;
        $group_items[$id] = $title;
      }
    }
    return $group_items;
  }

  /**
   * Handler for creating mailchimp campaign.
   *
   * @param string $audience_id
   *   Mailchimp audience ID.
   * @param string $from_name
   *   From name for campaign.
   * @param string $from_email
   *   From email for campaign.
   * @param string $subject
   *   Subject for campaign.
   * @param string $body
   *   Body for campaign.
   * @param array $rules
   *   Array of segments.
   *
   * @return string|NULL
   *   Mailchimp request id or NULL of fail.
   */
  public function createCampaign($audience_id, $from_name, $from_email, $subject, $body, $rules) {
    $mailchimp = $this->getConnection();
    $campaignId = NULL;
    $response = NULL;

    try {
      $request = $this->buildCampaignRequest($audience_id, $from_name, $from_email, $subject, $rules);
      $response = $mailchimp->campaigns->create($request);
      $campaignId = $response->id;

      $mailchimp->campaigns->setContent($campaignId, [
        'html' => $body,
      ]);

    }
    catch (\Exception $error) {
      $message = $this->t('Error: @message<br />Details: @details<br /> Campaign details: <br /><pre>@campaign</pre><br /> Response: <br /><pre>@response</pre>', [
        '@message' => $this->t('Failed to create campaign.'),
        '@details' => $error->getMessage(),
        '@campaign' => print_r($campaignId, TRUE),
        '@response' => print_r($response, TRUE),
      ]);

      $this->logger->error($message);
    }

    return $campaignId;
  }

  /**
   * Callback to create campaign request structure.
   *
   * @param string $audience_id
   *   Mailchimp audience ID.
   * @param string $from_name
   *   From name for campaign.
   * @param string $from_email
   *   From email for campaign.
   * @param string $subject
   *   Subject for campaign.
   * @param array $rules
   *   Array of segments containing:
   *    'value': array of values
   *    'op': one of interestcontains (one of), interestcontainsall (all of),
   *          interestnotcontains (none of)
   *
   * @return array
   *   Mailchimp request array.
   */
  private function buildCampaignRequest($audience_id, $from_name, $from_email, $subject, $rules) {
    // "Interests" for group. "StaticSegment" for tags.
    $segments = [];
    foreach ($rules as $interestId => $rule) {
      $segments[] = [
        "condition_type" => "Interests",
        "field" => $interestId,
        "op" => $rule['op'],
        "value" => $rule['value'],
      ];
    }

    $request = [
      "type" => "regular",
      "recipients" => [
        "list_id" => $audience_id,
      ],
      "settings" => [
        "subject_line" => $subject,
        "preview_text" => 'Preview',
        "title" => $subject,
        "from_name" => $from_name,
        "reply_to" => $from_email,
        "use_conversation" => FALSE,
        "to_name" => "",
        "folder_id" => "",
        "authenticate" => FALSE,
        "auto_footer" => FALSE,
        "inline_css" => FALSE,
        "auto_tweet" => FALSE,
        "auto_fb_post" => [],
        "fb_comments" => FALSE,
        "template_id" => 0,
      ],
      "content_type" => "template",
    ];

    if (count($segments) > 0) {
      $request['recipients']['segment_opts'] =
        [
          "match" => "all",
          //"match" => "any",
          "conditions" => $segments,
        ];
    }

    return $request;
  }
}
