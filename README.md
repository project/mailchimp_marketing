# Mailchimp marketing

[Mailchimp](https://mailchimp.com/) email campaign service integration module.

## What is the difference?

As opposed to another [mailchimp](https://www.drupal.org/project/mailchimp) module
`Mailchimp marketing` uses the official php 
[mailchimp marketing](https://github.com/mailchimp/mailchimp-marketing-php) library.

## 2.0
* Breaking changes: class `Drupal\mailchimp_marketing\Controller\MailchimpController` was replaced with `Drupal\mailchimp_marketing\Mailchimp`.

## Features:

* Subscribing to content types

## Roadmap:

* Taxonomy integration: syncing groups and taxonomy terms
* Webform integration
